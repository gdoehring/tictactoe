#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <vector>
#include <memory>
#include "strategy/strategy.h"
#include "strategy/manualStrategy.h"

using namespace std;

/**
 * Generic player class.
 * Behaviour is determined by the passed strategy
 */
class Player {
    public:
        Player(const vector<char> * gamefield, const char&& passed_sign, Strategy *passed_strategy): 
            gamefield{gamefield}, sign{passed_sign}, strategy{passed_strategy} { }
        ~Player() {
            delete strategy;
        }
        int nextTurn(int last_move) {
            return strategy->getMove(gamefield, last_move);
        };
        char getSign(){ return sign; };

    private:
        // Pointer to the playfield. Const so that player cannot alter it
        const vector<char> * gamefield;

        // The sign of this player
        const char sign;

        // The strategy used
        Strategy *strategy;
};

#endif