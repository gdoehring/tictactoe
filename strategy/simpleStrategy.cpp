#include <iostream>
#include <list>
#include <algorithm>
#include <time.h>
#include "simpleStrategy.h"

using namespace std;

int SimpleStrategy::getMove(const vector<char> * gamefield, int lastmove) {

    // If we did a move before, check if we can win now
    if (lastmove_self > -1) {
        int doMove = checkForSingleEmptyField(gamefield, lastmove_self);

        // Do we need to take action?
        if (doMove > -1) {
            lastmove_self = doMove;
            return doMove;
        }
    }

    // Check if we need to prevent the enemy from winning
    if (lastmove > -1) {
        int doMove = checkForSingleEmptyField(gamefield, lastmove);

        // Do we need to take action?
        if (doMove > -1) {
            lastmove_self = doMove;
            return doMove;
        }
    }

    // Otherwise pick empty field at random
    lastmove_self = RandomStrategy::getMove(gamefield, lastmove);

    // Return the picked field
    return lastmove_self;
}

/**
 * This function checks wether in the column, row or diagonal (if applicable) of the
 * passed move there are two of the same signs and one free field. This would mean we need to take this
 * field.
 * NOTE: There is no check for the type of the sign. Because when this function is
 *      called to check for own win, two enemy signs and a free field cannot be found 
 *      because it is only checking rows, columns, diagonals where our move was in.
 *      The same is true for checking for enemy "prevent loosing" moves.
 */ 
int SimpleStrategy::checkForSingleEmptyField(const vector<char> * gamefield, int lastmove) {

    list<list<int>>::const_iterator it;
    // Go over all possible directions (rows, columns and diagonals)
    for(it = allDirections.begin(); it != allDirections.end(); it++) {

        list<int>::const_iterator result = find(it->begin(), it->end(), lastmove);

        // This is an direction the move is in (diagonal, row or column)
        if (result != it->end()) {
            // Check if there is exactly one space free and the other two are of the same type
            list<int>::const_iterator inner_it;
            int emptyField = -1;
            int numSigns = 0;
            char lastSign = ' ';

            // Go through these fields and check
            for (inner_it = it->begin(); inner_it != it->end(); inner_it++) {

                // if the field is empty, increase counter
                if (gamefield->at(*inner_it) == ' ') {
                    emptyField = *inner_it;
                
                // Check if we've not encountered a sign: store sign
                } else if (lastSign == ' ') {
                    lastSign = gamefield->at(*inner_it);
                    numSigns++;

                // If this sign is the same as before, increase counter.
                // If it is mixed do nothing
                } else if (gamefield->at(*inner_it) == lastSign) {
                    numSigns++;
                }
            }

            // If we have an empty field and 2 sign, this means we need to do the empty field
            if (emptyField != -1 && numSigns == 2) {
                return emptyField;
            }
        }
    }

    // We didn't find anything to do, return -1 to signal no action
    return -1;
  
}

int SimpleStrategy::getStrategyType() {
    return Strategy::STRATEGY_AI_SIMPLE;
}