#include <iostream>
#include <time.h>
#include "randomStrategy.h"

using namespace std;

int RandomStrategy::getMove(const vector<char> * gamefield, int lastmove) {

    // initialize random seed
    srand (time(NULL));

    // Collect all free fields
    vector<int> freeFields;
    for (int i = 0; i < gamefield->size(); i++) {
        if (gamefield->at(i) == ' ') {
            freeFields.push_back(i);
        }
    }

    if (freeFields.size() == 0) {
        throw "Field is full, cannot make turn";
    }

    // Pick one random free field
    int randomPick = rand() % freeFields.size();

    // Return the picked field
    return freeFields.at(randomPick);
}

int RandomStrategy::getStrategyType() {
    return Strategy::STRATEGY_AI_SIMPLE;
}