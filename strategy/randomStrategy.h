#ifndef RANDOM_STRATEGY_H
#define RANDOM_STRATEGY_H

#include <vector>
#include "strategy.h"

class RandomStrategy: public Strategy {

    public:
        int getMove(const vector<char> * gamefield, int lastmove);
        int getStrategyType();
};

#endif