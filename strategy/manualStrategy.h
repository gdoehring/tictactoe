#ifndef MANUAL_STRATEGY_H
#define MANUAL_STRATEGY_H

#include <vector>
#include "strategy.h"

class ManualStrategy: public Strategy {

    int getMove(const vector<char> * gamefield, int lastmove);
    int getStrategyType();
};

#endif