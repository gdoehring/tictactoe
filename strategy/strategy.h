#ifndef STRATEGY_H
#define STRATEGY_H

#include <vector>

using namespace std;

class Strategy {

    public:
        static const int STRATEGY_MANUAL = 1;
        static const int STRATEGY_AI_SIMPLE = 2;
        virtual int getMove(const vector<char> * gamefield, int lastmove) = 0;
        virtual int getStrategyType() = 0;
};

#endif