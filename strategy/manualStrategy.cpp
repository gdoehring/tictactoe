#include <iostream>
#include "manualStrategy.h"

using namespace std;

int ManualStrategy::getMove(const vector<char> * gamefield, int lastmove) {

    // Prompt the user to select a field where to move
    bool moveNotFinished = true;
    int selectedField = -1;

    while (moveNotFinished) {
        char input;
        
        cin >> input;

        if (isdigit(input)) {
            selectedField = atoi(&input);

            // Check if the range is correct
            if ( selectedField < 1 || selectedField > 9) {
                cout << "ungueltige Eingabe. Bitte Wert zwischen 1 und 9 eingeben";
                continue;
            }

            // Remove the input by 1 to be in line with gamefield
            selectedField--;

            // Check if this field is free
            if (gamefield->at(selectedField) != ' ') {
                cout << "Feld nicht frei, bitte anderes waehlen" << endl;
                continue;
            }
            moveNotFinished = false;
        } else {
            cout << "Eingabe ist keine Zahl, bitte erneut eingeben" << endl;
        }
    }

    return selectedField;
}

int ManualStrategy::getStrategyType() {
    return Strategy::STRATEGY_MANUAL;
}