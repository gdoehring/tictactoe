#ifndef SIMPLE_STRATEGY_H
#define SIMPLE_STRATEGY_H

#include <vector>
#include <list>
#include "randomStrategy.h"

class SimpleStrategy: public RandomStrategy {

    public:
        int getMove(const vector<char> * gamefield, int lastmove);
        int getStrategyType();

    private:
        int checkForSingleEmptyField(const vector<char> * gamefield, int lastmove);
        int lastmove_self = -1;
        const list<int> diagonal1{0, 4, 8};
        const list<int> diagonal2{2, 4, 6};
        const list<int> column1{0, 3, 6};
        const list<int> column2{1, 4, 7};
        const list<int> column3{2, 5, 8};
        const list<int> row1{0, 1, 2};
        const list<int> row2{3, 4, 5};
        const list<int> row3{6, 7, 8};

        const list<list<int>> allDirections{
            diagonal1, diagonal2,
            column1, column2, column3,
            row1, row2, row3
        };
};

#endif