#ifndef GAME_H
#define GAME_H

#include <vector>
#include "player.h"

class Game {
    public:
        Game();
        ~Game();
        void run();
        bool hasWon();
        static const int GAMEMODE_NONE = 0;
        static const int GAMEMODE_HUMAN_AI_RANDOM = 1;
        static const int GAMEMODE_HUMAN_AI_SIMPLE = 2;
        static const int GAMEMODE_HUMAN_HUMAN = 3;
        static const int GAMEMODE_AI_AI = 4;
        static const int GAMEMODE_FINISHED = 5;
        static const int GAMEMODE_END = 6;

    private:
        vector<char> gamefield;
        unique_ptr<Player> currentPlayer;
        unique_ptr<Player> nextPlayer;
        int gamemode = Game::GAMEMODE_NONE;
        void getGameMode();
        void gameLoop();
        void printGamefield();
        void printGameModeInstruction();
        void resetGamefield();
};

#endif