#include <iostream>
#include "game.h"
#include "player.h"
#include "strategy/strategy.h"
#include "strategy/manualStrategy.cpp"
#include "strategy/randomStrategy.cpp"
#include "strategy/simpleStrategy.cpp"

using namespace std;

Game::Game() {
    // Initialize the gamefield
    gamefield = vector<char>(9);
}

Game::~Game() {
    currentPlayer.reset(nullptr);
    nextPlayer.reset(nullptr);
}

/**
 * Main function for the game. Runs in a loop until it is finished
 */
void Game::run() {

    // Keep the game running until it should end
    while (gamemode != Game::GAMEMODE_END) {

        // Print instructions for selecting the gamemode
        printGameModeInstruction();

        // Get desired gamemode
        getGameMode();

        // If we want to abort the game
        if (gamemode == Game::GAMEMODE_END) {
            continue;
        }

        // Reset the game field and print it
        resetGamefield();
        printGamefield();

        // Setup the players according to the gamemode
        // Unique pointer is used for easy swap
        if (gamemode == Game::GAMEMODE_HUMAN_AI_SIMPLE) {
            currentPlayer = unique_ptr<Player>(new Player(&gamefield, move('X'), new ManualStrategy()));
            nextPlayer = unique_ptr<Player>(new Player(&gamefield, move('O'), new SimpleStrategy()));
        } else if (gamemode == Game::GAMEMODE_HUMAN_AI_RANDOM) {
            currentPlayer = unique_ptr<Player>(new Player(&gamefield, move('X'), new ManualStrategy()));
            nextPlayer = unique_ptr<Player>(new Player(&gamefield, move('O'), new RandomStrategy()));
        } else if (gamemode == Game::GAMEMODE_HUMAN_HUMAN) {
            currentPlayer = unique_ptr<Player>(new Player(&gamefield, move('X'), new ManualStrategy()));
            nextPlayer = unique_ptr<Player>(new Player(&gamefield, move('O'), new ManualStrategy()));
        } else if (gamemode == Game::GAMEMODE_AI_AI) {
            currentPlayer = unique_ptr<Player>(new Player(&gamefield, move('X'), new SimpleStrategy()));
            nextPlayer = unique_ptr<Player>(new Player(&gamefield, move('O'), new SimpleStrategy()));
        } else {
            throw "Unsupported gamemode";
        }


        // Gamemode was decided, let's start
        gameLoop();

        // After the game and before a new game, do cleanup
        currentPlayer.reset(nullptr);
        nextPlayer.reset(nullptr);
        gamemode = Game::GAMEMODE_NONE;

        std::cout << endl << "Spiel beendet, X fuer beenden, andere Taste neues Spiel, dann ENTER druecken" << endl;

        char input;
        cin >> input;

        // If the user wants to end
        if (input == 'X' || input == 'x') {
            gamemode = Game::GAMEMODE_END;
        }
    }
}

void Game::gameLoop() {

    int lastMove = -1;
    int currentTurn = 0;
    while (gamemode != Game::GAMEMODE_END && gamemode != Game::GAMEMODE_FINISHED) {

        std::cout << endl << "Spieler (" << currentPlayer->getSign() << ") am Zug" << endl;

        int playerTurn = currentPlayer->nextTurn(lastMove);

        // Assign the move to the field
        gamefield.at(playerTurn) = currentPlayer->getSign();
        lastMove = playerTurn;

        // Print the gamefield
        printGamefield();

        // Check if we have a winner
        if (hasWon()) {
            std::cout << endl << "Spieler " << currentPlayer->getSign() << " hat gewonnen";
            gamemode = Game::GAMEMODE_FINISHED;
        }

        // Swap the players
        currentPlayer.swap(nextPlayer);
        currentTurn++;

        // After 9 turns, it is a draw
        if (currentTurn == 9) {
            gamemode = Game::GAMEMODE_FINISHED;
            std::cout << endl << "Unenteschieden" << endl;
        }
    }
}

bool Game::hasWon() {

    // 8 possibilities to check: 3 rows, 3 columns, 2 diagonals

    // rows
    if (gamefield.at(0) != ' ' && gamefield.at(0) == gamefield.at(1) && gamefield.at(1) == gamefield.at(2)) {
        return true;
    }
    if (gamefield.at(3) != ' ' && gamefield.at(3) == gamefield.at(4) && gamefield.at(4) == gamefield.at(5)) {
        return true;
    }
    if (gamefield.at(6) != ' ' && gamefield.at(6) == gamefield.at(7) && gamefield.at(7) == gamefield.at(8)) {
        return true;
    }

    // columns
    if (gamefield.at(0) != ' ' && gamefield.at(0) == gamefield.at(3) && gamefield.at(3) == gamefield.at(6)) {
        return true;
    }
    if (gamefield.at(1) != ' ' && gamefield.at(1) == gamefield.at(4) && gamefield.at(4) == gamefield.at(7)) {
        return true;
    }
    if (gamefield.at(2) != ' ' && gamefield.at(2) == gamefield.at(5) && gamefield.at(5) == gamefield.at(8)) {
        return true;
    }

    // diagonals
    if (gamefield.at(0) != ' ' && gamefield.at(0) == gamefield.at(4) && gamefield.at(4) == gamefield.at(8)) {
        return true;
    }
    if (gamefield.at(2) != ' ' && gamefield.at(2) == gamefield.at(4) && gamefield.at(4) == gamefield.at(6)) {
        return true;
    }

    return false;
}

void Game::getGameMode() {
    // Next we want to get the gamemode (human vs. ai, human vs. human or ai vs. ai)
    while (gamemode == Game::GAMEMODE_NONE) {
        char input;
        cin >> input;
        
        if (input == 'X' || input == 'x') {
            std::cout << "Spiel beendet" << endl;
            gamemode = Game::GAMEMODE_END;
            continue;
        }

        // Human vs. ai
        if (input == '1') {
            gamemode = Game::GAMEMODE_HUMAN_AI_RANDOM;
            continue;
        }

        if (input == '2') {
            gamemode = Game::GAMEMODE_HUMAN_AI_SIMPLE;
            continue;
        }

        // Human vs. human
        if (input == '3') {
            gamemode = Game::GAMEMODE_HUMAN_HUMAN;
            continue;
        }

        // ai vs. ai
        if (input == '4') {
            gamemode = Game::GAMEMODE_AI_AI;
            continue;
        }

        // If we reached this point, input was not recognized
        std::cout << endl << "Eingabe nicht erkannt, bitte erneut eingeben." << endl;
    }
}

void Game::printGamefield() {

    // Translate the gamemode into a string
    string gamemode_name;
    switch (gamemode) {
        case Game::GAMEMODE_HUMAN_AI_RANDOM:
        case Game::GAMEMODE_HUMAN_AI_SIMPLE:
            gamemode_name = "Mensch gegen Computer";
            break;

        case Game::GAMEMODE_HUMAN_HUMAN:
            gamemode_name = "Mensch gegen Mensch";
            break;

        case Game::GAMEMODE_AI_AI:
            gamemode_name = "Computer gegen Computer";
            break;

        default:
            throw "Unknown gamemode";
    }

    // Clear console
    system("cls");

    // Write header part
    cout << "TicTacToe" << '(' << gamemode_name.c_str() << ')' << endl << endl
        << "|---|---|---|" << endl
        << "| ";
    
    // Print all the fields
    for (int i = 0; i < gamefield.size(); i++) {

        // Either print sign or number of the field
        if (gamefield.at(i) != ' ') {
            cout << gamefield.at(i);
        } else {
            cout << (i + 1);
        }
        cout << " | ";

        // End of row reached, print new line and separator
        if (i % 3 == 2) {
            cout << endl << "|---|---|---|" << endl;

            // When this was not the last row, add beginning of next row
            if (i < 6) {
                cout << "| ";
            }
        }
    }
    cout << endl;
}

void Game::printGameModeInstruction() {
    system("cls");
    cout << "TicTacToe" << endl << endl
        << "Waehlen Sie den Spielmodus: " << endl
        << "1.: Mensch gegen Computer (Zufall)" << endl
        << "2.: Mensch gegen Computer (Leicht)" << endl
        << "3.: Mensch gegen Mensch" << endl
        << "4.: Computer (Leicht) gegen Computer (Leicht)" << endl
        << "X.: Beenden" << endl;
}

void Game::resetGamefield() {
    for (int i = 0; i < 9; i++) {
        gamefield.at(i) = ' ';
    }
}