#include <iostream>
#include <vector>
#include <string>
#include "game.cpp"

using namespace std;

// Main entry point
int main()
{
    Game game;
    game.run();
}